require 'thor'
require_relative 'lib/game_reporter'
require_relative 'lib/game_runner'

class Game < Thor

  desc 'play NUMBER_OF_CHILDREN ELIMINATION_NUMBER', 'Number of children, elimination number'
  long_desc <<-LONGDESC
    With a defined number of children,
    starting from the first child,
    count up to an elimination number,
    eliminate the child at that number,
    then start counting again,
    continue until one child remains as the winner'
  LONGDESC
  def play(number_of_children, elimination_number)

    begin
      game_runner = GameRunner.new(number_of_children, elimination_number)
      game_reporter = GameReporter.new($stdout)
      game_runner.run
      game_reporter.report(game_runner)
    rescue RuntimeError => exception
      $stdout.puts exception.message
    end

  end

end

Game.start