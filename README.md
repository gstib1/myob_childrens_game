Grant Tibbey

- Run `ruby game.rb play NUMBER_OF_CHILDREN ELIMINATION_NUMBER` or `bin/game play NUMBER_OF_CHILDREN ELIMINATION_NUMBER`
- Run cucumber tests: `rake features`
- Run unit tests: `rake tests`

