require_relative 'child'

class Circle

  def initialize(number_of_children)
    @children = (1..number_of_children).collect { |i| Child.new(i) }
  end

  def next
    @children.push(@children.shift)
  end

  def current
    @children.first
  end

  def remove
    @children.shift
  end

  def length
    @children.length
  end

end
