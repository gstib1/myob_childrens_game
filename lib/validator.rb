class Validator

  def self.validate_elimination_number(elimination_number)
    unless elimination_number =~ /^(([1-9])|([1-9][0-9])|100)$/
      raise 'ERROR: game play was called with ELIMINATION_NUMBER not an integer between 1 and 100'
    end
  end

  def self.validate_number_of_children(number_of_children)
    unless number_of_children =~ /^(([2-9])|([1-9][0-9])|100)$/
      raise 'ERROR: game play was called with NUMBER_OF_CHILDREN not an integer between 2 and 100'
    end
  end

end