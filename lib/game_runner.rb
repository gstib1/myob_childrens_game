require_relative 'circle'
require_relative 'validator'

class GameRunner

  attr_reader :eliminated_children

  def initialize(number_of_children, elimination_number)
    Validator::validate_number_of_children(number_of_children)
    Validator::validate_elimination_number(elimination_number)
    @circle = Circle.new(number_of_children.to_i)
    @elimination_number = elimination_number.to_i
    @eliminated_children = []
  end

  def run
    until winner
      eliminate_child
    end
  end

  def winner
    @circle.current if @circle.length == 1
  end

  private

  def eliminate_child
    if @circle.length > 1
      (1..@elimination_number-1).each{ @circle.next }
      @eliminated_children << @circle.remove
    end
  end

end