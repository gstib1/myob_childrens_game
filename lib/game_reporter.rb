class GameReporter

  def initialize(output)
    @output = output
  end

  def report(gaming_runner)
    @output.puts "Eliminated Children Id Sequence : #{format_ids(gaming_runner.eliminated_children) }"
    @output.puts "Winning Child Id : #{gaming_runner.winner.id}"
  end

  private

  def format_ids(children)
    children.map(&:id).join(',')
  end

end