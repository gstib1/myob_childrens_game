require 'test/unit'
require_relative '../child'

class ChildTest < Test::Unit::TestCase

  def test_constructor_should_set_id
    child = Child.new(1)
    assert_equal 1, child.id
  end

end