require 'test/unit'
require_relative '../validator'

class ValidatorTest < Test::Unit::TestCase

  def test_alpha_should_raise_number_of_children_error
    should_raise_number_of_children_error{
      Validator::validate_number_of_children('a')
    }
  end

  def test_one_should_raise_number_of_children_error
    should_raise_number_of_children_error{
      Validator::validate_number_of_children('1')
    }
  end

  def test_one_hundred_one_should_raise_number_of_children_error
    should_raise_number_of_children_error{
      Validator::validate_number_of_children('101')
    }
  end

  def test_one_point_nine_should_raise_number_of_children_error
    should_raise_number_of_children_error{
      Validator::validate_number_of_children('1.9')
    }
  end

  def test_two_should_not_raise_number_of_children_error
      Validator::validate_number_of_children('2')
  end

  def test_one_hundred_should_not_raise_number_of_children_error
    Validator::validate_number_of_children('100')
  end

  def test_alpha_should_raise_elimination_number_error
    should_raise_elimination_number_error{
      Validator::validate_elimination_number('a')
    }
  end

  def test_zero_should_raise_elimination_number_error
    should_raise_elimination_number_error{
      Validator::validate_elimination_number('0')
    }
  end

  def test_one_hundred_one_should_raise_elimination_number_error
    should_raise_elimination_number_error{
      Validator::validate_elimination_number('101')
    }
  end

  def test_point_nine_should_raise_elimination_number_error
    should_raise_elimination_number_error{
      Validator::validate_elimination_number('0.9')
    }
  end

  def test_one_should_not_raise_elimination_number_error
    Validator::validate_elimination_number('1')
  end

  def test_one_hundred_should_not_raise_elimination_number_error
    Validator::validate_elimination_number('100')
  end

  def should_raise_number_of_children_error(&block)
    assert_raise(RuntimeError.new('ERROR: game play was called with NUMBER_OF_CHILDREN not an integer between 2 and 100'), &block)
  end

  def should_raise_elimination_number_error(&block)
    assert_raise(RuntimeError.new('ERROR: game play was called with ELIMINATION_NUMBER not an integer between 1 and 100'), &block)
  end


end