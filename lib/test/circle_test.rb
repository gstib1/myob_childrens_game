require 'test/unit'
require_relative '../circle'

class CircleTest < Test::Unit::TestCase

  def setup
    @child_count = 2
    @circle = Circle.new(@child_count)
  end

  def test_should_create_two_children
    assert_equal(2, @circle.length)
  end

  def test_should_sequence_children_starting_at_one
    assert_equal(1, @circle.remove.id)
    assert_equal(2, @circle.remove.id)
  end

  def test_current_is_first_item_in_array
    assert_equal(1, @circle.current.id)
  end

  def test_current_is_second_item_in_array_after_next_called
    @circle.next
    assert_equal(2, @circle.current.id)
  end

  def test_current_is_the_first_item_in_array_once_next_is_called_the_same_times_as_children
    (1..@child_count).each{ @circle.next }
    assert_equal(1, @circle.current.id)
  end

  def test_remove_removes_item_from_array
    @circle.remove
    assert_equal(1, @circle.length)
  end

  def test_remove_removes_the_current_item_in_array
    current_child = @circle.current
    remove_child = @circle.remove
    assert_equal(current_child, remove_child)
  end




end