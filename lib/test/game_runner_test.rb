require 'test/unit'
require 'mocha/setup'
require_relative '../game_runner'
require_relative '../circle'
require_relative '../child'


class GameRunnerTest < Test::Unit::TestCase

  def setup
    @game_runner = GameRunner.new('4', '2')
  end

  def test_gaming_area_is_initialized_with_no_eliminated_children
    assert_empty(@game_runner.eliminated_children)
  end

  def test_gaming_area_is_initialized_with_no_winner
    assert_nil(@game_runner.winner)
  end

  def test_run_eliminates_second_fourth_and_third_child
    @game_runner.run
    assert_equal(2, @game_runner.eliminated_children[0].id)
    assert_equal(4, @game_runner.eliminated_children[1].id)
    assert_equal(3, @game_runner.eliminated_children[2].id)
  end

  def test_run_child_sets_winner_to_fourth
    @game_runner.run
    assert_equal(1, @game_runner.winner.id)
  end

  def test_should_validate_elimination_number
    assert_raise(RuntimeError.new('ERROR: game play was called with ELIMINATION_NUMBER not an integer between 1 and 100')) {
      GameRunner.new('2', 'a')
    }
  end

  def test_should_validate_number_of_children
    assert_raise(RuntimeError.new('ERROR: game play was called with NUMBER_OF_CHILDREN not an integer between 2 and 100')) {
      GameRunner.new('a', '1')
    }
  end

end