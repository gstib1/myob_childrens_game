require 'test/unit'
require 'mocha/setup'
require_relative '../game_reporter'
require_relative '../child'

class GameReporterTest < Test::Unit::TestCase

  def setup
    @game_runner = mock
    @output = mock
  end

  def test_reports_in_the_correct_format
    stub([Child.new(1)], Child.new(2))
    @output.expects(:puts).with('Eliminated Children Id Sequence : 1')
    @output.expects(:puts).with('Winning Child Id : 2')
    report
  end


  def test_report_places_comma_between_more_than_one_eliminated_child_id
    stub([Child.new(3),Child.new(1)], Child.new(2))
    @output.expects(:puts).with('Eliminated Children Id Sequence : 3,1')
    @output.expects(:puts).with('Winning Child Id : 2')
    report
  end


  def stub(eliminated_children, winner)
    @game_runner.stubs(:eliminated_children).returns(eliminated_children)
    @game_runner.stubs(:winner).returns(winner)
  end

  def report
    game_reporter = GameReporter.new(@output)
    game_reporter.report(@game_runner)
  end


end