Feature: Play Game
  With a defined number of children
  Counting to a specified elimination number
  The game should show the sequence that children are eliminated by Id and
  The winning child's Id

  Scenario: Play the game with three children counting to two
    When I run `game play 3 2`
    Then the output should contain:
    """
    Eliminated Children Id Sequence : 2,1
    Winning Child Id : 3
    """

  Scenario: Play the game with no children
    When I run `game play`
    Then the output should contain:
    """
    ERROR: game play was called with no arguments
    """

  Scenario Outline: Play the game with a invalid NUMBER_OF_CHILDREN
    When I run `<cmd>`
    Then the output should contain:
    """
    ERROR: game play was called with NUMBER_OF_CHILDREN not an integer between 2 and 100
    """
  Examples:
  | cmd             |
  | game play a 1   |
  | game play 1 1   |
  | game play 101 1 |
  | game play 1.9 1 |

  Scenario Outline: Play the game with a invalid ELIMINATION_NUMBER
    When I run `<cmd>`
    Then the output should contain:
    """
    ERROR: game play was called with ELIMINATION_NUMBER not an integer between 1 and 100
    """
  Examples:
  | cmd             |
  | game play 2 a   |
  | game play 2 0   |
  | game play 2 101 |
  | game play 2 0.9 |
